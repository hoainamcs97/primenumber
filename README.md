Project title
This is the decentralized web application that takes in a number and returns to the user the highest prime number lower than the input number.

Features
Truffle
Solidity
Node
Mongoose
Express
React

Platforms to Deploy
DigitalOcean

Environment variables
MONGO_URI=
PORT=
NODE_ENV=
BLOCKCHAIN_HTTP_URI=
PRIME_NUMBER_NAME=
PRIME_NUMBER_ADDRESS=

Build Server from Source
#Install dependencies
yarn install
#Start server
yarn start

Build Web Interface from Source
cd client
#Install dependencies
yarn install
#Start server
yarn start