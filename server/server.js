require('dotenv').config();
const mongoose = require('mongoose');
const winston = require('../config/winston');
const app = require('./app');
const https = require('https');
const fs = require('fs');

console.log(process.env.MONGO_URI)
mongoose
  .connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    autoIndex: true
  })
  .then(() => {
    winston.info('MongoDB Connected Successfully...');
  })
  .catch((err) => winston.error(err));

let port = process.env.PORT || 8080;

let server = app;

server.listen(port, () => {
  winston.info(`Server listening on port ${port}`);
});
server.on('error', (error) => {
  winston.error('Error: ', error);
});
