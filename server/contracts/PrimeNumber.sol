pragma solidity 0.5.4;


contract PrimeNumber {

    event EventLog(
        string EventName,
        string ParameterName1,
        uint256 ParameterValue1,
        string ParameterName2,
        uint256 ParameterValue2
    );

    mapping(uint256 => uint256) public prime;

    function addPrime(uint limit) public {
        require(limit > 2);
        if (limit >= 100) {
            for (uint j = limit-1; j > 0; j--) {
                if (_isPrime(j)) {
                    prime[limit] = j;
                    emit EventLog("Find-Prime", "Input-Number", limit, "Output-Prime-Number", j);
                    break;
                }
            }
        } else {
            for (uint j = limit-1; j > 0; j--) {
                if (_ifPrime(j)) {
                    prime[limit] = j;
                    emit EventLog("Find-Prime", "Input-Number", limit, "Output-Prime-Number", j);
                    break;
                }
            }
        }
    }

    function getPrime(uint256 limit) public view returns (uint256) {
        return prime[limit];
    }

    function _sqrt(uint num) private pure returns (uint) {
        if (0 == num) {
            return 0;
        }
        uint x = (num / 2) + 1;
        uint y = (x + (num / x)) / 2;
        while (y < x) {
            x = y;
            y = (x + (num / x)) / 2;
        }
        return x;
    }

    function _ifPrime(uint _num) private pure returns (bool) {
        for (uint i = 2; i < _num; i++) {
            if (_num % i == 0) return false;
        }
        return true;
    }

    function _isPrime(uint _num) private pure returns (bool) {
        uint256 s = _sqrt(_num);
        for (uint i=2; i <= s; i++) {
            if (_num % i == 0) return false;
        }
        return true;
    }
}
