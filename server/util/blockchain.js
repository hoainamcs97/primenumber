const EthereumTx = require('ethereumjs-tx'); //must be version 1.3.7
const util = require('ethjs-util');
const RLP = require('rlp');
const { Keccak } = require('sha3');
const web3 = require('../../config/web3');
const fs = require('fs');

const createAccount = () => web3.eth.accounts.create();

const getTxStatus = async (tx_id) => {
  try {
    const receipt = await web3.eth.getTransactionReceipt(tx_id);
    return receipt && (receipt.status === '0x01' || receipt.status === true);
  } catch (error) {
    return false;
  }
};

const web3GetTransactionCount = (web3, ...args) => new Promise((resolve, reject) => {
    web3.eth.getTransactionCount(...args, (error, doc) => {
      if (error) {
        reject(error);
      } else {
        resolve(doc);
      }
    });

});

const web3SendSignedTransaction = (web3, ...args) => new Promise((resolve, reject) => {
    web3.eth.sendSignedTransaction(...args, (error, doc) => {
      if (error) {
        reject(error);
      } else {
        resolve(doc);
      }
    }).on('receipt', async() => {
    });
});

const estimateGas = async (from, nonce, to, data) => {
  try {
    const gasLimit = await web3.eth.estimateGas({
      "from"      : from,
      "nonce"     : nonce,
      "to"        : to,
      "data"      : data
    })

    return gasLimit;
  } catch (error) {
    throw error;
  }
};

const getContractInstance = async (contractName, contractAddress) => {
  try {
    const contractJSON = fs.readFileSync(`${__dirname}/../build/contracts/${contractName}.json`);
    const contractJSONParse = JSON.parse(contractJSON);
    const contractABI = contractJSONParse['abi'];
    const contractDeployed = new web3.eth.Contract(contractABI, contractAddress);

    return contractDeployed;
  } catch (error) {
    throw error;
  }
};

const getTxData = async (contractName, contractAddress, functionName, ...paramLst) => {
  try {
    const contractJSON = fs.readFileSync(`${__dirname}/../build/contracts/${contractName}.json`);
    const contractJSONParse = JSON.parse(contractJSON);
    const contractABI = contractJSONParse['abi'];
    const contractDeployed = new web3.eth.Contract(contractABI, contractAddress);
    const data = contractDeployed.methods[`${functionName}`]( ...paramLst).encodeABI();
    return data;
  } catch (error) {
    throw error;
  }
};

const getTxParam = async (contractName, contractAddress, functionName, from, nonce, gasPrice, gasLimit, ...paramLst) => {
  try {
    const data = await getTxData(contractName, contractAddress, functionName, ...paramLst);
    const txParam = {
      from: from,
      nonce: nonce,
      gasPrice: gasPrice, // web3.utils.toHex(web3.utils.toWei('100', 'gwei')),
      gasLimit: gasLimit,
      to: contractAddress,
      data: data,
    };
    return txParam;
  } catch (error) {
    throw error;
  }
};

const getSignedTx = (rawTx, privateKey) => {
  try {
    const tx = new EthereumTx(rawTx, { chain: 4 });

    const stripedPrivateKey = util.stripHexPrefix(privateKey);
    const privateKeySign = Buffer.from(stripedPrivateKey, 'hex');

    tx.sign(privateKeySign);

    const serializedTx = tx.serialize();
    const signedTxToSend = '0x' + serializedTx.toString('hex');

    return signedTxToSend;
  } catch (error) {
    throw error;
  }
};

const verifySender = (signedTx, address) => {
  const tx = new EthereumTx(signedTx);
  const sender = tx.getSenderAddress().toString('hex');
  return sender === address.toLowerCase();
};

const getContractAddress = async (
  creator, //address of doc model factory
  creator_nonce
) => {
  const response =
    (await '0x') +
    Keccak(256)
      .update(RLP.encode([creator, web3.utils.toHex(creator_nonce)]))
      .digest('hex')
      .substring(24);
  return response;
};

const readData = async (contractName, contractAddress, functionName, ...paramLst) => {
  const contract = await getContractInstance(contractName, contractAddress);
  const result = await contract.methods[`${functionName}`]( ...paramLst).call();
  return result;
};

module.exports = {
  createAccount,
  getTxStatus,
  web3GetTransactionCount,
  web3SendSignedTransaction,
  estimateGas,
  getContractInstance,
  getTxData,
  getTxParam,
  getSignedTx,
  verifySender,
  getContractAddress,
  readData
};
