const util = require('util')

const findOne = (collection, ...args) => util.promisify(collection.findOne).bind(collection)(...args)

const findOneAndUpdate = (collection, ...args) => util.promisify(collection.findOneAndUpdate).bind(collection)(...args)

const find = (collection, ...args) => util.promisify(collection.find).bind(collection)(...args)

const advanceFind = async (collection, findCondition, sortCondition, limit) => {
  return await collection.find(findCondition).sort(sortCondition).limit(limit)
}

const update = (collection, ...args) => util.promisify(collection.update).bind(collection)(...args)

const insertMany = (collection, ...args) => util.promisify(collection.insertMany).bind(collection)(...args)

const create = (collection, ...args) => util.promisify(collection.create).bind(collection)(...args)

const toArray = (collection) => util.promisify(collection.toArray).bind(collection)()

const deleteOne = (collection) => util.promisify(collection.deleteOne).bind(collection)()


module.exports = {
  findOne,
  findOneAndUpdate,
  find,
  advanceFind,
  update,
  insertMany,
  create,
  toArray,
  deleteOne
}
