const CONSTANTS = require('../../config');
const Logger = require('../util/logger');
const BLOCKCHAIN = require('../util').blockchain;
const Mongo = require('../helpers').mongo;

const web3 = CONSTANTS.web3;
const sc = CONSTANTS.sc;
const ADD_PRIME = 'addPrime(uint256)'
const GET_PRIME = 'getPrime(uint256)'
const GASLIMIT = 15000000

const addNumber = async (num) => {
  try {
    const bcAccount = BLOCKCHAIN.createAccount();
    const bcAddress = bcAccount.address;
    const bcPrikey = bcAccount.privateKey;
    const data = await BLOCKCHAIN.getTxData(sc.NAME, sc.ADDRESS, ADD_PRIME, num);
    const rawTx = await BLOCKCHAIN.getTxParam(sc.NAME, sc.ADDRESS, ADD_PRIME, bcAddress, 0, 0, GASLIMIT, num);
    const signedTx = await BLOCKCHAIN.getSignedTx(rawTx, bcPrikey);
    const response = web3.utils.keccak256(signedTx);
    BLOCKCHAIN.web3SendSignedTransaction(web3, signedTx);
    Mongo.setHistory({txID:response, input:num});

    return response;
  } catch (error) {
    Logger.log('addNumber error', error.response);
    if (error && error.response) throw error.response;
    throw Error(error.response || error);
  }
};

const getPrime = async (limit) => {
  try {
    const prime = await BLOCKCHAIN.readData(sc.NAME, sc.ADDRESS, GET_PRIME, limit);
    return prime;
  } catch (error) {
    Logger.log('getPrime error', error.response);
    if (error && error.response) throw error.response;
    throw Error(error.response || error);
  }
};

const getHistory = async(limit) => {
  try {
    const data = await Mongo.latestHistory(limit);
    return data;
  } catch (error) {
    Logger.log('getHistory error', error.response);
    if (error && error.response) throw error.response;
    throw Error(error.response || error);
  }
};

const getTxStatus = async(txID) => {
  try {
    const status = await BLOCKCHAIN.getTxStatus(txID);
    return status;
  } catch (error) {
    Logger.log('getHistory error', error.response);
    if (error && error.response) throw error.response;
    throw Error(error.response || error);
  }
}

module.exports = {
  addNumber,
  getPrime,
  getHistory,
  getTxStatus
};
