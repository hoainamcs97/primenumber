const express = require('express');
const router = express.Router();
const PrimeController = require('../controllers/primeNumber');

// Add new number
router.post(
  '/add',
  PrimeController.addNum
);

// Get one prime by number
router.post(
  '/prime',
  PrimeController.getPrime
);

// Get history by limit
router.post(
  '/logs',
  PrimeController.getHistory
);

// Get status of Transaction by txID
router.post(
  '/tx-status',
  PrimeController.getTxStatus
);

module.exports = router;
