const RESPONSE = require('../middlewares/response')
const ERRORCODE = require('../constants/errorCode').ErrorCode
const Logger = require('../util/logger')
const PRIME_NUMBER = require('../services/primeNumber')

const addNum = async (req, res) => {
  try {
    const { num } = req.body
    if (!num) {
      ERRORCODE.MISSING_FIELD.message = 'Missing "num" field'
      throw ERRORCODE.MISSING_FIELD
    }

    const data = await PRIME_NUMBER.addNumber(num)

    if (!data) return RESPONSE.message(res, ERRORCODE.DATA_NOT_EXISTED)

    return RESPONSE.message(res, {...ERRORCODE.SUCCESSFUL, data})

  } catch (error) {
    Logger.error('add number error: ', error)
    if (error && error.errorCode && error.name && error.message) {
      return RESPONSE.message(res, error)
    }
    return RESPONSE.message(res, ERRORCODE.ERROR_SERVER)
  }
}
const getPrime = async (req, res) => {
  try {
    const { limit } = req.body

    if (!limit) {
      ERRORCODE.MISSING_FIELD.message = 'Missing "limit" field'
      throw ERRORCODE.MISSING_FIELD
    }

    const data = await PRIME_NUMBER.getPrime(limit)
    if (!data) return RESPONSE.message(res, ERRORCODE.DATA_NOT_EXISTED)

    return RESPONSE.message(res, { ...ERRORCODE.SUCCESSFUL, data })

  } catch (error) {
    Logger.error('get prime error: ', error)
    if (error && error.errorCode && error.name && error.message) {
      return RESPONSE.message(res, error)
    }
    return RESPONSE.message(res, ERRORCODE.ERROR_SERVER)
  }
}

const getHistory = async (req, res) => {
  try {
    const { limit } = req.body

    if (!limit) {
      ERRORCODE.MISSING_FIELD.message = 'Missing "limit" field'
      throw ERRORCODE.MISSING_FIELD
    }

    const data = await PRIME_NUMBER.getHistory(limit)
    if (!data) return RESPONSE.message(res, ERRORCODE.DATA_NOT_EXISTED)

    return RESPONSE.message(res, { ...ERRORCODE.SUCCESSFUL, data })

  } catch (error) {
    Logger.error('get history error: ', error)
    if (error && error.errorCode && error.name && error.message) {
      return RESPONSE.message(res, error)
    }
    return RESPONSE.message(res, ERRORCODE.ERROR_SERVER)
  }
}

const getTxStatus = async (req, res) => {
  try {
    const { txID } = req.body

    if (!txID) {
      ERRORCODE.MISSING_FIELD.message = 'Missing "txID" field'
      throw ERRORCODE.MISSING_FIELD
    }

    const data = await PRIME_NUMBER.getTxStatus(txID)
    if (!data) return RESPONSE.message(res, ERRORCODE.DATA_NOT_EXISTED)

    return RESPONSE.message(res, { ...ERRORCODE.SUCCESSFUL, data })

  } catch (error) {
    Logger.error('get history error: ', error)
    if (error && error.errorCode && error.name && error.message) {
      return RESPONSE.message(res, error)
    }
    return RESPONSE.message(res, ERRORCODE.ERROR_SERVER)
  }
}

module.exports = {
  addNum,
  getPrime,
  getHistory,
  getTxStatus
}
