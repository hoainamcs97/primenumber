var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const helmet = require('helmet');
const cors = require('cors');
var compression = require('compression');
const mountRoutes = require('./routes');
const winston = require('../config/winston');
const apiErrors = require('./helpers/apiError');

require('dotenv').config();

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(
  logger('combined', {
    stream: {
      write: function (message) {
        winston.info(message);
      }
    }
  })
);

app.use(helmet());
app.use(cors());
app.use(compression());
app.use(express.json({ limit: '20mb' }));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
//app.use(express.static(path.join(__dirname, 'public')));
mountRoutes(app);
// Serve static assets if in production
if (process.env.NODE_ENV === 'production') {
  app.use(express.static(path.join(__dirname, 'build')));

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'build', 'index.html'));
  });
}

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  console.log(req)
  if (err.message === 'Not Found') {
    return res
      .status(404)
      .json(apiErrors.not_found.withDetails('Path not found.'));
  }

  // log error
  let errToLog = err.realError ? err.realError : err;
  let errToReturn = err.realError ? err.userError : err;

  console.error('ERROR:', errToLog);
  console.log(req)
  winston.error(
    `${errToLog.message}
    STACK: ${errToLog.stack}
    Header: ${JSON.stringify(req.headers)}
    Params: ${JSON.stringify(req.params)}
    Query: ${JSON.stringify(req.query)}
    Body: ${JSON.stringify(req.body)}
    Return to user: ${JSON.stringify(errToReturn)}`
  );

  const status = errToReturn.code / 100;
  return res.status(status).json({
    error: errToReturn
  });
});

module.exports = app;
