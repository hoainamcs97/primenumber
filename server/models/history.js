const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const HistorySchema = new Schema({
  txID: {
    type: String,
    unique: true,
    required: true
  },
  input: {
    type: Number,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
}, {collection: 'History'});

HistorySchema.index({ txID: 1 }, { unique: true })
const History = mongoose.model('History', HistorySchema);

module.exports = History
