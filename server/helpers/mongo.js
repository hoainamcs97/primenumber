const {dbhelper} = require('../util')
const {History} = require('../models')

// Add new document of History
const setHistory = async(data) => {
    dbhelper.create(History, data);
}

// Get latest History by Limit
const latestHistory = async(limit) => {
  const findCondition = { input: { $gt: 2 } }
  const sortCondition = {date: -1}
  const data = await dbhelper.advanceFind(History, findCondition, sortCondition, limit);
  return data;
}

module.exports = {
  setHistory,
  latestHistory
}
