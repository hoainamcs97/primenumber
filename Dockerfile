FROM node:14 as base
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
EXPOSE 5000

FROM base as development
ENV NODE_ENV development
COPY package.json package-lock.json ./
RUN npm install
RUN npm install ethereumjs-tx@1.3.7
RUN npm install dotenv
RUN npm install winston
RUN npm install app-root-path
RUN npm install cookie-parser
RUN npm install morgan
RUN npm install helmet
RUN npm install cors
RUN npm install standard
RUN npm install log4js
RUN npm install web3
RUN npm install sha3
COPY .env .eslintrc .eslintrc.js .babelrc index.js nodemon.json webpack.config.babel.js webpack.config.dev.js webpack.config.prod.js webpack.config.server.js ./
COPY client ./client
COPY Intl ./Intl
COPY server ./server
CMD ["yarn", "start"]


