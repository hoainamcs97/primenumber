require('dotenv').config();

const sc = {
  ADDRESS: process.env.PRIME_NUMBER_ADDRESS,
  NAME: process.env.PRIME_NUMBER_NAME
}

module.exports = sc;

