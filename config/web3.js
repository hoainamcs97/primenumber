const Web3 = require('web3');
require('dotenv').config();

const web3 = new Web3(
  new Web3.providers.HttpProvider(process.env.BLOCKCHAIN_HTTP_URI)
);

module.exports = web3;

