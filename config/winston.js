var appRoot = require('app-root-path');
var winston = require('winston');

var options = {
  file: {
    level: 'info',
    filename: `${appRoot}/logs/app.log`,
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    colorize: false
  },
  console: {
    level: 'debug',
    handleExceptions: true,
    json: true,
    colorize: true
  }
};

var logger = new winston.createLogger({
  transports: [
    new winston.transports.File(options.file),
    new winston.transports.Console(options.console)
  ],
  exitOnError: false,
  format: winston.format.combine(
    winston.format.label({
      label: 'default'
    }),
    winston.format.timestamp(),
    winston.format.printf(info => {
      return `${info.timestamp} - ${
        info.label
      } [${info.level.toLocaleUpperCase()}] - ${info.message}`;
    })
  )
});

module.exports = logger;

