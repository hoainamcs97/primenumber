require('dotenv').config();

const db = {
  MONGO_URI: process.env.MONGO_URI
}

module.exports = db;

