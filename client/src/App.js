import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import InputNumber from "./view/Input";
import "./css/index.css"
import {Container} from "reactstrap"
function App() {
  return (
    <div className="d-flex justify-content-center align-items-center full-screen">
      <Container>
      <InputNumber />
      </Container>
    </div>
  );
}

export default App;
