import axios from "axios";
import { Formik } from 'formik';
import React from "react";
import SweetAlert from "react-bootstrap-sweetalert";
import { Button, Col, Input, Row } from "reactstrap";
import * as Yup from "yup";
import { LINK_API } from "../constants/index";
import TableData from "./Table";

const InputNumber = () => {
    const [alert, setAlert] = React.useState(null);
    const hideAlert = () => {
        setAlert(null)
    }
    const handleAddValue = (number) => {
        setAlert(
            <SweetAlert
            title='Calculating prime number ...'
            showCancel={false}
            showConfirm={false}
            >
             <img alt='loading' src='https://res.cloudinary.com/agridential/image/upload/c_scale,w_150/v1607952697/Logo/gif-leaf-loading-gif-MAIN_iyxr8u.gif'/>
            </SweetAlert>
        )
        let dataSample = {
            "num": number
        }
        // handleCheck(number)
        axios.post(LINK_API.ADD_NUMBER, dataSample)
        .then(res=>{
            if(res.data.data.errorCode === 1){
             let tx = res.data.data.data; 
             let tempTX = {
                "txID": tx
             };
             setAlert(
                <SweetAlert
                title='Checking transaction ...'
                showCancel={false}
                showConfirm={false}
                >
                 <img alt='loading' src='https://res.cloudinary.com/agridential/image/upload/c_scale,w_150/v1607952697/Logo/gif-leaf-loading-gif-MAIN_iyxr8u.gif'/>
                </SweetAlert>
            )
             setTimeout(()=>{
                axios.post(LINK_API.CHECK_TX, tempTX)
                .then(responseData=>{
                    if(responseData.data.data.errorCode === 1){
                          if(responseData.data.data.data === true){
                               handleCheck(number)
                          }else{
                           setAlert(
                               <SweetAlert
                               error
                               title='Failed'
                               showCancel={false}
                               showConfirm={true}
                               onConfirm={hideAlert}
                               >
                               </SweetAlert>  
                           )
                          }
                    }else{
                       setAlert(
                           <SweetAlert
                           error
                           title='Failed'
                           showCancel={false}
                           showConfirm={true}
                           onConfirm={hideAlert}
                           >
                           </SweetAlert>  
                       )
                    }
                })
                .catch(err=>{
                    console.log(err);
                    setAlert(
                       <SweetAlert
                       error
                       title='Failed'
                       showCancel={false}
                       showConfirm={true}
                       onConfirm={hideAlert}
                       >
                       </SweetAlert>  
                   )
                })
             }, 10000)
            
            }
        })
        .catch(err=>{
            setAlert(
                <SweetAlert
                error
                title='Failed'
                showCancel={false}
                showConfirm={true}
                onConfirm={hideAlert}
                >
                </SweetAlert>  
            )
        })
    }
    const handleCheck = (number) => {
        let data = {
            'limit': number
        }
        axios.post(LINK_API.CHECK_PRIME, data)
        .then(res=>{
            if(res.data.data.errorCode === 1){
                if(res.data.data.data === "0"){
                    handleAddValue(number)
                }else{
                    handleLog()
                   setAlert(
                    <SweetAlert
                    success
                    title='Prime number is'
                    showCancel={false}
                    showConfirm={true}
                    onConfirm={hideAlert}
                    >
                        <h1 className='font-weight-bold'>{res.data.data.data}</h1>
                    </SweetAlert>  
                   )
                }
            }else{
                setAlert(
                    <SweetAlert
                    error
                    title='Failed'
                    showCancel={false}
                    showConfirm={true}
                    onConfirm={hideAlert}
                    >
                    </SweetAlert>  
                )
            }
        })
        .catch(err=>{
            console.log(err)
            setAlert(
                <SweetAlert
                error
                title='Failed'
                showCancel={false}
                showConfirm={true}
                onConfirm={hideAlert}
                >
                </SweetAlert>  
            )
        })
    }
    const [dataTable, setData] = React.useState([])
    const handleLog = async () => {

        let resdata = await axios.post(LINK_API.GET_LOG,{'limit': 5});
        console.log(resdata.data.data.data)
        setData(resdata.data.data.data)
    }
    React.useEffect(()=>{
        handleLog();
    },[])
    return (
        <Row>
            <Col md='5'>
                <Formik
                    initialValues={{ number: '' }}
                    validationSchema={
                        Yup.object().shape({
                          number: Yup.number()
                            .min(3)
                            .required()
                        })
                      }
                    onSubmit={(values, actions) => {
                        handleCheck(values.number);
                    }}
                >
                    {props => (
                        <form onSubmit={props.handleSubmit}>
                            <div className='d-flex flex-column '>
                                <h1 className='font-weight-bold'>PRIME NUMBER</h1>
                                <Input
                                    placeholder='Input prime number'
                                    onChange={props.handleChange}
                                    type='number'
                                    name='number'
                                    value={props.values.number}
                                />
                                {props.errors.number && <div id="feedback" className='text-danger'>{props.errors.number}</div>}
                                <div className='d-flex py-2'>
                                    <Button color='success' type='submit'>Get</Button>
                                    {/* <Button color='info' className='ml-2'>Get</Button> */}
                                </div>
                            </div>
                        </form>
                    )}
                </Formik>
            </Col>
            <Col md='7'>
                <TableData data={dataTable} />
            </Col>
            {alert}
        </Row>

    )
}
export default InputNumber;