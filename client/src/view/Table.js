import moment from "moment";
import React from "react"
import ReactTable from 'react-table-6'
import 'react-table-6/react-table.css'
import { Button, ModalHeader, ModalBody, ModalFooter, Modal } from "reactstrap"
import "../css/index.css"
const TableData = (props) => {
    const data = props.data;
    const [dataTable, setData] = React.useState([])
    const [transactionID, setTX] = React.useState('')
    React.useEffect(() => {
        if (data) {
            genTable(data)
        }
    }, [data])
    const genTable = async (list) => {
        let temp = await list.map((item) => {
            return ({
                ...item,
                input: <div className='w-100 text-center'>{item.input}</div>,
                time: <div className='w-100 text-center'>{moment(item.date).format('DD/MM/YYYY HH:mm:ss')}</div>,
                txID: <span onClick={() => toggle(item.txID)} className='text-info'>{item.txID}</span>
            })
        })
        setData(temp)
    }
    const columns = [{
        Header: 'Number',
        accessor: 'input',
    }, {
        Header: 'Timestamp',
        accessor: 'time',
    }, {
        Header: 'TxID',
        accessor: 'txID',

    }]
    const [modal, setModal] = React.useState(false);

    const toggle = async (tx) => {
        await setTX(tx)
        setModal(!modal);
    }

    return (
        <>
        <h1 className='font-weight-bold'>LATEST ACTIONS</h1>
            <ReactTable
                data={dataTable}
                columns={columns}
                defaultPageSize={5}
                noDataText='No data'
                showPagination={false}
            />
            <Modal isOpen={modal} toggle={toggle} className='fullWidthModal'>
                <ModalHeader toggle={toggle}>Transaction Detail</ModalHeader>
                <ModalBody style={{ width: '100vw' }}>
                    <iframe title="Blockchain" style={{ height: '70vh', width: '100%' }} src={'https://blockchain.agridential.vn/vpi/' + transactionID} />
                </ModalBody>
                <ModalFooter>
                    <Button color='danger' outline onClick={toggle}>Close</Button>{' '}
                </ModalFooter>
            </Modal>
        </>
    )
}
export default TableData;
