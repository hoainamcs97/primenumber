
const HOST = 'http://178.128.222.145:5000';

const LINK_API = {
    ADD_NUMBER: HOST + '/api/v1/add',
    CHECK_PRIME: HOST + '/api/v1/prime',
    CHECK_TX: HOST + '/api/v1/tx-status',
    GET_LOG: HOST + '/api/v1/logs'
}

export {
    LINK_API
}

export default {
    LINK_API
}
